import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Mini Calculadora';

  // numero_1: string = '0';
  // numero_2: string = '0';
  numero_1: number = 0;
  numero_2: number = 0;
  resultado: number = 0;

  sumar(): void {
    // this.resultado = parseInt(this.numero_1) + parseInt(this.numero_2);
    this.resultado = this.numero_1 + this.numero_2;
  }

  restar(): void {
    this.resultado = this.numero_1 - this.numero_2;
  }
}
